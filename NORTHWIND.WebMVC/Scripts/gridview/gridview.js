﻿
$.fn.extend({
    gridView: function (opt) {
        if (opt == undefined) {
            return {};
        }
        if (opt.columns == undefined || opt.columns == null) {
            opt.columns = [];
        }
        
        var page = 1;
        var pageSize = 10;
        var criterias = [];
        var strOrder = opt.defaultSortExpression
        var sortDirection = true;
        var pageCount = 1;
        var rowCount = 0;

        var grid = $(this);
        var eid = grid.attr('id');
        var content = ['<div class="form-horizontal">',
            '<div id="sch" class="hid">',
            '<div class="form-group">',
            '<div class="col-md-1 control-label" style="text-align: left">Criteria</div>',
            '<div class="col-md-2">',
            '<select id="' + eid +'_ddl-criteria" class="form-control"> ',
            '</select>',
            '</div>',
            '<div class="col-md-3">',
            '<input type="text" id="' + eid + '_txtValue" name="' + eid +'_txtValue" class="form-control" />',
            '</div>',
            '<div class="col-md-1">',
            '<button id="' + eid +'_btnSearch" type="button" class="btn btn-primary btn-sm"><span class="fa fa-search"></span> Search</button>',
            '</div>',
            '</div>',
            '</div>',
            '<div class="form-group">',
            '<div id="' + eid +'_crtList" class="col-md-12">',
            '</div>',
            '</div>',
            '<div class="form-group">',
                '<div class="col-md-12">',
                    '<div style="overflow-x: scroll">',
                        '<table id="' + eid +'_tablegrid" class="table table-bordered table-striped">',
                            '<thead></thead>',
                            '<tbody></tbody>',
                        '</table>', 
                    '</div>',
                '</div>',
                '<div id="backdrop-overlay" >','<div id="loader" ></div>','<div id="overlay" style=""></div>','</div>',
            '</div>',
            '<div class="form-group">',
                '<div class="col-md-2" style="padding-top: 4px">',
                '<span>Total Data: </span><span id="' + eid +'_totalRow">100</span>',
            '</div>',
            '<div class="col-md-10" style="text-align: right">',
                '<span>Show Per Page: </span>',
                '<select id="' + eid + '_ddlPageSize" name="' + eid +'_ddlPageSize" style="height: 30px;box-shadow: none;border-color: #d2d6de;border: 1px solid #ccc;padding: 2px 8px; font-size: 14px; line-height: 1.42857143; color: #555;">',
                    '<option value="5">5</option>',
                    '<option value="10">10</option>',
                    '<option value="25">25</option>',
                    '<option value="50">50</option>',
                    '<option value="100">100</option>',
                '</select>&nbsp;',
                '<span> Page: </span>',
                '<input id="' + eid + '_txtPage" type="text" name="' + eid +'_txtPage" class="numeric digit" style="height: 30px;max-width: 50px; box-shadow: none;border-color: #d2d6de;border: 1px solid #ccc;padding: 2px 8px; font-size: 14px; line-height: 1.42857143; color: #555;" />',
                '&nbsp;/&nbsp;<span id="' + eid +'_pageCount" style="margin-right: 20px">12</span>',
                '<div class="btn-group">',
                    '<button type="button" id="' + eid +'_btnPrev" class="btn btn-primary btn-sm" style="margin-top: -3px"><span class="glyphicon glyphicon-chevron-left"></span></button>',
                    '<button type="button" id="' + eid +'_btnNext" class="btn btn-primary btn-sm" style="margin-top: -3px; "><span class="glyphicon glyphicon-chevron-right"></span></button>',
                '</div>',
            '</div>',
            '</div>',
            '</div>'].join('');
        grid.append(content);
        $('.numeric').numeric();

        //method list
        var func = {
            bindingIn: function () {
                $('#' + eid + '_tablegrid tr th').removeClass('sortedasc').removeClass('sorteddesc');
                $('#' + eid + '_tablegrid tr th[sortkey="' + strOrder + '"][sortable="true"]').addClass(sortDirection ? 'sortedasc' : 'sorteddesc');
                $('#' + eid + '_ddlPageSize').val(pageSize).prop('disabled', rowCount == 0);
                $('#' + eid + '_txtPage').val(page).prop('disabled', rowCount == 0);
                $('#' + eid + '_totalRow').html(rowCount);
                $('#' + eid + '_pageCount').html(pageCount);
                $('#' + eid + '_btnPrev').prop('disabled', page == 1 || rowCount == 0);
                $('#' + eid + '_btnNext').prop('disabled', page == pageCount || rowCount == 0);
            },
            displayData: function () {
                func.progressOn();
                var data = {
                    page: page,
                    pageSize: pageSize,
                    criteria: criterias,
                    order: strOrder + ' ' + (sortDirection?'ASC':'DESC')
                };
                var settings = {
                    url: opt.url,
                    method: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function (res) {
                        var tbody = '';
                        var rows = res.data.rows;
                        for (var i = 0; i < rows.length; i++) {
                            tbody = tbody.concat('<tr>');
                            for (var j = 0; j < opt.columns.length; j++) {
                                col = opt.columns[j];
                                if (col.render) {
                                    tbody = tbody.concat('<td>', col.render(rows[i][col.data]), '</td>'); 
                                } else { 
                                    tbody = tbody.concat('<td ' + (col.type === 'numeric' ? 'style="text-align: right"':'')+'>', rows[i][col.data], '</td>');
                                }
                            }
                            tbody = tbody.concat('</tr>');
                        }
                        $('#' + eid + '_tablegrid tbody').html(tbody); 
                        rowCount = res.data.rowCount;
                        pageCount = res.data.pageCount;
                        func.bindingIn();
                        func.progressOff();
                    },
                    error: function (err) {
                        console.log('Error: ');
                        console.log(err);
                    },
                    complete: function (data) {
                        func.progressOff(); 
                    }
                };
                $.ajax(settings);               
            },
            initHead: function () {
                if ((strOrder === undefined || strOrder === '') && opt.columns.length > 0) {
                    strOrder = opt.columns[0].data;
                }
                var content = '', thead = '<tr>';
                var col;
                for (var i = 0; i < opt.columns.length; i++) {
                    col = opt.columns[i];
                    if (col.header !== undefined && col.header !== '') {
                        content = content.concat('<option value="', col.data, '">', col.header, '</option>');
                    }
                    thead = thead.concat('<th style="min-width: ' + (col.header.length > 0 ? ((col.header.length+4)*10):'60')  + 'px;" sortkey="', col.data, '" ', (col.sortable ? 'sortable="true"' : ''), '>', col.header, '</th>');                    
                }
                thead = thead.concat('</tr>');
                $('#' + eid +'_tablegrid thead').html(thead);
                $('#' + eid +'_ddl-criteria').html(content); 
            },
            renderCriteria: function () {
                var ctl = $('#' + eid +'_crtList');
                ctl.empty();
                var item = '<ul style="list-style:none">';
                for (var i = 0; i < criterias.length; i++) {
                    item = item.concat('<li>', 'Search \'', '<span style="color: darkblue">', criterias[i].criteria, '</span>', '\' contains ', '\'', '<span style="color: darkblue">', criterias[i].value, '</span>', '\'', ' <a href="javascript:" index="' + i + '" style="color: red; text-decoration: none">Remove</a>', '</li>');
                }
                item = item.concat('</ul>');
                ctl.append(item); 
                //event remove
                $('#' + eid + '_crtList ul li a').on('click', function (e) {
                    var index = $(e.target).attr('index');
                    criterias.splice(index, 1);
                    func.renderCriteria();
                    func.displayData();
                });
            },
            progressOn: function () {
                $('#backdrop-overlay').fadeIn(100);
            },
            progressOff: function () {
                $('#backdrop-overlay').fadeOut(100);
            }
        };

        //init state
        func.bindingIn();

        //init header and ddl criteria 
        func.initHead();

        //display data
        func.displayData();

        //event list
        $('#' + eid + '_btnSearch').on('click', function () {
            var criteria = $('#' + eid + '_ddl-criteria').val();
            var value = $('#' + eid + '_txtValue').val();
            if (criteria == undefined || criteria === null || criteria === '' || value == undefined || value === null || value === '') {
                return;
            }
            criterias.push({ criteria: criteria, value: value });
            func.renderCriteria();
            page = 1;
            func.displayData();
            $('#' + eid + '_txtValue').val('');
        });

        $('#' + eid + '_txtValue').on('keypress', function (evt) {
            if (evt.keyCode == 13) {
                $('#' + eid + '_btnSearch').click();
            }
        });
         
        $('#' + eid + '_tablegrid thead tr th[sortable="true"]').on('click', function (evt) {
            strOrder = $(this).attr('sortkey');
            sortDirection = !sortDirection;
            func.displayData();
        });

        $('#' + eid +'_ddlPageSize').on('change', function (evt) { 
            pageSize = $(evt.target).val();
            page = 1;
            func.displayData();
        });

        $('#' + eid + '_txtPage').on('keyup', function (evt) {
            var p = $(evt.target).val();
            if (p == page || p == '' || p === '' || p == NaN) {
                return;
            }
            page = p;
            if (page > pageCount) {
                page = pageCount;
            }
            if (page < 1) {
                page = 1;
            }
            func.displayData();
        });

        $('#' + eid +'_btnPrev').on('click', function () {
            --page;
            func.displayData(); 
        });

        $('#' + eid + '_btnNext').on('click', function () { 
            ++page;
            func.displayData();
        });

        var exp = {
            reload: function () {
                func.displayData(); 
            }
        };

        return exp;
    }
});