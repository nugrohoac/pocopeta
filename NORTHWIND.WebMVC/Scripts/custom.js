﻿function switchSearch() {
    $('#sch').toggleClass("hid");
}
function showPopUp() {
        $('#myModalLabel').html("Add New Customer");
        $('#CustomerID').val("");
        $('#CompanyName').val("");
        $('#ContactName').val("");
        $('#ContactTitle').val("");
        $('#Address').val("");
        $('#City').val("");
        $('#Region').val("");
        $('#PostalCode').val("");
        $('#Country').val("");
        $('#Phone').val("");
        $('#Fax').val("");
        $('#btnUpdate').hide();
        $('#btnAdd').show();
        $('#CustomerID').css('border-color', 'lightgrey');
        $('#CompanyName').css('border-color', 'lightgrey');
        $('#ContactName').css('border-color', 'lightgrey');
        $('#ContactTitle').css('border-color', 'lightgrey');
        $('#Address').css('border-color', 'lightgrey');
        $('#City').css('border-color', 'lightgrey');
        $('#Region').css('border-color', 'lightgrey');
        $('#PostalCode').css('border-color', 'lightgrey');
        $('#Country').css('border-color', 'lightgrey');
        $('#Phone').css('border-color', 'lightgrey');
        $('#Fax').css('border-color', 'lightgrey');
        $('#myModal').modal('show');
}

function Add() {
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        CustomerID: $('#CustomerID').val(),
        CompanyName: $('#CompanyName').val(),
        ContactName: $('#ContactName').val(),
        Address: $('#Address').val(),
        City: $('#City').val(),
        Region: $('#Region').val(),
        PostalCode: $('#PostalCode').val(),
        Country: $('#Country').val(),
        Phone: $('#Phone').val(),
        Fax: $('#Fax').val()
    };
    $.ajax({
        url: "/Customers/Save",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            console.log(result);
            console.log('success');
            console.log(empObj);
            $('#myModal').modal('hide');

            loadData();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function validate() {
    var isValid = true;
    if ($('#CustomerID').val().trim() == "") {
        $('#CustomerID').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#CustomerID').css('border-color', 'lightgrey');
    }
    if ($('#CompanyName').val().trim() == "") {
        $('#CompanyName').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#CompanyName').css('border-color', 'lightgrey');
    }
    if ($('#ContactName').val().trim() == "") {
        $('#ContactName').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ContactName').css('border-color', 'lightgrey');
    }
    if ($('#ContactTitle').val().trim() == "") {
        $('#ContactTitle').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ContactTitle').css('border-color', 'lightgrey');
    }
    if ($('#Address').val().trim() == "") {
        $('#Address').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Address').css('border-color', 'lightgrey');
    }
    if ($('#City').val().trim() == "") {
        $('#City').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#City').css('border-color', 'lightgrey');
    }
    if ($('#Region').val().trim() == "") {
        $('#Region').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Region').css('border-color', 'lightgrey');
    }
    if ($('#PostalCode').val().trim() == "") {
        $('#PostalCode').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#PostalCode').css('border-color', 'lightgrey');
    }
    if ($('#Country').val().trim() == "") {
        $('#Country').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Country').css('border-color', 'lightgrey');
    }
    if ($('#Phone').val().trim() == "") {
        $('#Phone').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Phone').css('border-color', 'lightgrey');
    }
    if ($('#Fax').val().trim() == "") {
        $('#Fax').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#Fax').css('border-color', 'lightgrey');
    }
    return isValid;
}

function Delete() {
    var ans = confirm("Are you sure you want to delete this Record?");
    if (ans) {
        $.ajax({
            url: "/Customers/Delete/" + ID,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}