﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NORTHWIND.WebMVC.Models
{
    public class CustomerView
    {
            [Required]
            [Display(Name = "Customer ID")]
            public string CustomerID { get; set; }

            [Required]
            [Display(Name = "Company Name")]
            public string CompanyName { get; set; }

            [Required]
            [Display(Name = "Contact Name")]
            public string ContactName { get; set; }

            [Required]
            [Display(Name = "Contact Title")]
            public string ContactTitle { get; set; }

            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }

            [Required]
            [Display(Name = "City")]
            public string City { get; set; }

            [Required]
            [Display(Name = "Region")]
            public string Region { get; set; }

            [Required]
            [Display(Name = "Postal Code")]
            public string PostalCode { get; set; }

            [Required]
            [Display(Name = "Country")]
            public string Country { get; set; }

            [Required]
            [Display(Name = "Phone")]
            public string Phone { get; set; }

            [Required]
            [Display(Name = "Fax")]
            public string Fax { get; set; }
    }
}