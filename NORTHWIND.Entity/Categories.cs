﻿using System; 
using PetaPoco;

namespace NORTHWIND.Entity
{
    [TableName("Categories")]
    [PrimaryKey("CategoryID")] 
    [ExplicitColumns]
    [Serializable]
    public class Categories
    {
        [Column]
        public int CategoryID { get; set; }

        [Column]
        public string CategoryName { get; set; }

        [Column]
        public string Description { get; set; }

        [Column]
        public byte[] Picture { get; set; } 
    }
}
