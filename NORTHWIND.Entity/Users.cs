using System;  

namespace NORTHWIND.Entity
{
	[PetaPoco.TableName("Users")]
	[PetaPoco.PrimaryKey("UserID")]
	[PetaPoco.ExplicitColumns]
    [Serializable]
	public class Users 
	{		
		#region Property
		
		[PetaPoco.Column]	
		public string UserID{ get; set;}
		
		[PetaPoco.Column]	
		public string UserName{ get; set;}
		
		[PetaPoco.Column]	
		public string UserPassword{ get; set;}
		
		[PetaPoco.Column]	
		public string UserEmail{ get; set;}
		
		[PetaPoco.Column]	
		public string UserPosition{ get; set;}
		
		[PetaPoco.Column]	
		public string UserManager{ get; set;}
		
		[PetaPoco.Column]	
		public bool UserStatus{ get; set;}
		#endregion		
	}
}
