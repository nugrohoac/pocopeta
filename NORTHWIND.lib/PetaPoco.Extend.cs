﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace PetaPoco
{ 
    public class Page
    {
        /// <summary>
		/// The current page number contained in this page of result set 
		/// </summary>
		public long CurrentPage
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of pages in the full result set
        /// </summary>
        public long TotalPages
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of records in the full result set
        /// </summary>
        public long TotalItems
        {
            get;
            set;
        }

        /// <summary>
        /// The number of items per page
        /// </summary>
        public long ItemsPerPage
        {
            get;
            set;
        }

        public DataTable DataTable { get; set; }
    }

    public partial class Database
    {
        public Page Page(long page, long itemsPerPage, Sql sql)
        {
            return Page(page, itemsPerPage, sql.SQL, sql.Arguments);
        }

        public Page Page(long page, long itemsPerPage, string sql, params object[] args)
        {
            string sqlCount, sqlPage;
            BuildPageQueries((page - 1) * itemsPerPage, itemsPerPage, sql, ref args, out sqlCount, out sqlPage);
            return Page(page, itemsPerPage, sqlCount, args, sqlPage, args); 
        }

        public Page Page(long page, long itemsPerPage, string sqlCount, object[] countArgs, string sqlPage, object[] pageArgs)
        {
            // Save the one-time command time out and use it for both queries
            var saveTimeout = OneTimeCommandTimeout;

            // Setup the paged result
            var result = new Page
            {
                CurrentPage = page,
                ItemsPerPage = itemsPerPage,
                TotalItems = ExecuteScalar<long>(sqlCount, countArgs)
            };

            result.TotalPages = result.TotalItems / itemsPerPage;

            if ((result.TotalItems % itemsPerPage) != 0)
                result.TotalPages++;

            OneTimeCommandTimeout = saveTimeout;

            result.DataTable = FetchDataTable(new Sql(sqlPage, pageArgs));

            return result;
        }

        void BuildPageQueries(long skip, long take, string sql, ref object[] args, out string sqlCount, out string sqlPage)
        { 
            SQLParts parts;
            if (!Provider.PagingUtility.SplitSQL(sql, out parts))
                throw new Exception("Unable to parse SQL statement for paged query");

            sqlPage = _provider.BuildPageQuery(skip, take, parts, ref args);
            sqlCount = parts.SqlCount;
        }

        public int FetchCount<T>(Sql sql)
        {
            string sqlCount, sqlPage;
            string strSql = sql.SQL;
            object[] args = sql.Arguments;
            BuildPageQueries<T>(0, 0, strSql, ref args, out sqlCount, out sqlPage);
            return ExecuteScalar<int>(sqlCount, args);
        }

        public DataTable FetchDataTable<T>(Sql oSql)
        {
            string sql = oSql.SQL;
            if (EnableAutoSelect)
                sql = AutoSelectHelper.AddSelectClause<T>(_provider, sql, _defaultMapper);

            OpenSharedConnection();
            try
            {
                using (var cmd = CreateCommand(_sharedConnection, sql, oSql.Arguments))
                {
                    IDataReader r;
                    try
                    {
                        r = cmd.ExecuteReader();
                        OnExecutedCommand(cmd);
                        DataTable dt = new DataTable("Result");
                        dt.Load(r);
                        return dt;
                    }
                    catch (Exception x)
                    {
                        if (OnException(x))
                            throw;
                    }
                }
            }
            finally
            {
                CloseSharedConnection();
            }
            return null;
        }

        public DataTable FetchDataTable(Sql oSql)
        {
            string sql = oSql.SQL;
            OpenSharedConnection();
            try
            {
                using (var cmd = CreateCommand(_sharedConnection, sql, oSql.Arguments))
                {
                    IDataReader r;
                    try
                    {
                        r = cmd.ExecuteReader();
                        OnExecutedCommand(cmd);
                        DataTable dt = new DataTable("Result");
                        dt.Load(r);
                        return dt;
                    }
                    catch (Exception x)
                    {
                        if (OnException(x))
                            throw;
                    }
                }
            }
            finally
            {
                CloseSharedConnection();
            }
            return null;
        }

        public DataTable PageDataTable<T>(long page, long itemsPerPage, string sqlCount, object[] countArgs, string sqlPage, object[] pageArgs)
        {
            // Save the one-time command time out and use it for both queries
            var saveTimeout = OneTimeCommandTimeout;

            // Setup the paged result
            var result = new Page<T>
            {
                CurrentPage = page,
                ItemsPerPage = itemsPerPage,
                TotalItems = ExecuteScalar<long>(sqlCount, countArgs)
            };
            result.TotalPages = result.TotalItems / itemsPerPage;

            if ((result.TotalItems % itemsPerPage) != 0)
                result.TotalPages++;

            OneTimeCommandTimeout = saveTimeout;

            // Get the records
            //result.Items = FetchDataTable<T>(sqlPage, pageArgs);

            // Done
            return FetchDataTable<T>(new Sql(sqlPage, pageArgs));
        }

        public DataTable PageDataTable<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            string sqlCount, sqlPage;
            BuildPageQueries<T>((page - 1) * itemsPerPage, itemsPerPage, sql, ref args, out sqlCount, out sqlPage);
            return PageDataTable<T>(page, itemsPerPage, sqlCount, args, sqlPage, args);
        }

        public DataTable PageDataTable<T>(long page, long itemsPerPage, Sql sql)
        {
            return PageDataTable<T>(page, itemsPerPage, sql.SQL, sql.Arguments);
        }
    }
}
