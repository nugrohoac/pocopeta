﻿using NORTHWIND.Entity;
using PetaPoco;
using System;
using System.Text.RegularExpressions;

namespace NORTHWIND.Facade
{
    public class CustomersFacade : BaseCRUD<Customers>
    {
        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Customers] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public new void Insert(Customers obj)
        {
            DB.Insert("Customers", "CustomerID", false, obj);
        }

        public new bool DataInsert(Customers cus, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(cus);
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }

        public bool hapus(string id)
        {

            return true;
        }


        public bool DataDelete(string id, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                var cus = GetItemByID(id);
                if (cus == null) throw new Exception("Data not found!");
                using (var scope = DB.GetTransaction())
                {
                    DB.Delete<Customers>(new Sql().Where("CustomerID = @0", id));
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = Regex.Replace(ex.Message.Replace("'", ""), @"\t|\n|\r", ""); return false; }
            return true;
        }

    }
}
