﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NORTHWIND.Entity;
using PetaPoco;

namespace NORTHWIND.Facade
{
    public class OrdersFacade: BaseCRUD<Orders>
    {
        public Page<OrdersView> GetPageByCriteria(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = @"SELECT * FROM (SELECT [OrderID]
                          ,a.[CustomerID]
	                      ,[CompanyName][CustomerName]
                          ,a.[EmployeeID]
	                      ,c.[FirstName] + ' '+c.[LastName] [EmployeeName]
                          ,[OrderDate]
                          ,[RequiredDate]
                          ,[ShippedDate]
                          ,[ShipVia]
                          ,[Freight]
                          ,[ShipName]
                          ,[ShipAddress]
                          ,[ShipCity]
                          ,[ShipRegion]
                          ,[ShipPostalCode]
                          ,[ShipCountry]
                      FROM [dbo].[Orders] a 
                      INNER JOIN [dbo].[Customers] b ON b.CustomerID = a.CustomerID
                      INNER JOIN [dbo].[Employees] c ON c.EmployeeID = a.EmployeeID)x";
            string where = " 1=1 "; 
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                where += string.Format("AND {0} LIKE @{1} ", key, i);
                cv[i] = string.Format("%{0}%", param[key]);
                i++;
            } 
            return DB.Page<OrdersView>(page, itemsPerPage, new Sql(sql).Where(where, cv).OrderBy(orderBy));
        }
         
    }
}
